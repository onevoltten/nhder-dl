if [ -z "$1" ]; then
    echo "Parameter invalid, exiting"
    exit 1
fi

re='^[0-9]+$'
if ! [[ $1 =~ $re ]]; then
    echo "Error: Not a number $1"
    exit 1
fi

# delete invalid folder
if ! ls download/${1}/*.jpg &>/dev/null; then
    if ! ls download/${1}/*.png &>/dev/null; then
        if ! ls download/${1}/*.gif &>/dev/null; then
            echo "Delete invalid [download/${1}]" >>./delete.log
            rm -rf "./download/${1}"
        fi
    fi
fi

# delete temp before processing
if [ -d "./download/temp/${1}" ]; then
    rm -rf "./download/temp/${1}"
fi

# if exists images
count=$(ls -1 download/${1}/*.jpg 2>/dev/null | wc -l)

# read json file
if [ -d "./download/${1}" ]; then
    if [ -f download/${1}/${1}.json ]; then
        cat download/${1}/${1}.json | while read line; do
            if [[ "$line" == *'"num_pages": '* ]]; then
                if [[ $count -lt ${line:13:-1} ]]; then
                    echo "[$count]" "[${line:13:-1}]"
                    echo "Delete invalid images! [${1}]"
                    rm -rf "./download/${1}"
                # else
                #     echo "Valid images [${1}]!"
                fi
                break
            fi
        done
    else
        echo "Delete invalid json! [${1}]"
        rm -rf "./download/${1}"
    fi
fi
# folder doesnt exist
if [ ! -d "./download/${1}" ]; then
    echo "Download [${1}]"
    node ./bin/nhder ${1} >>./download.log
    echo "Process [${1}]"
    nohup ./manage.sh ${1} >>./manage.log &
else
    echo "Exists [${1}]"
fi
