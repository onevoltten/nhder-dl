# nhder-dl

nhder doujinshi downloader

## Limitation
Please note this program should not be used for excessive downloads, rate limiting is applied.
Looking to download a large batch? Please contact an admin and a batch will be created.

## About
This is a modification of https://www.npmjs.com/package/nhder
This program is currently under development. Only basic logic is supported.

## Platform
Supports Windows, Mac, Linux.

## Installation

A native Windows executable is planned.

### Execute via nodejs

## Settings

```bash
npm install # Install dependencies
node nhder --setting
```

Displays the following interface

```text
nhder options

[1] Download path C:\nhentai # Download directory
[2] Download thread 16 # Download thread
[3] Analysis thread 5 # parsing thread
[4] Download timeout 20s # Download timeout
[5] Deduplication Enable # Deduplication
[6] Languages ​​filter english, english, japanese
[7] Proxy Disable # Proxy Settings
[0] Exit

Press a key [1...7 / 0]:
```

Enter the number of the corresponding setting item.
You must set the download directory before the first download.

- **Download directory**
  Please pay attention to the difference between the relative path and the absolute path, but don't worry, the absolute path will be displayed after entering the path to facilitate your inspection.  
  The directory does not need to be created manually, it will be created automatically when downloading images.
- **Download thread**
  That is, the number of pictures downloaded at the same time, the default is `8`, the minimum is `1`, and the maximum is `32`  
  The leftmost column when downloading the image is actually the thread number.
- ** parse thread**  
  The number of threads requesting API when parsing multi-page results. The default is `5`, the minimum is `1`, and the maximum is `10.  
  Try to reduce this value if a 503 error occurs during parsing
- **Download timeout**  
  If after a few seconds, a picture has not been downloaded, it will count as a timeout. If there is a timeout or network error, it will automatically retry. The default value is `30` seconds.  
  If the thread number is yellow when downloading the image, it means that this is a retry.  
  If the retry exceeds <10` times, it will be regarded as the download failure. At this time, the thread number will be marked in red and the program will be terminated.
- **Go to **  
  Once opened, duplicate books are filtered out when bulk downloading multiple categories in a category or search page (usually because a book has multiple translation languages)
- **Language filtering**  
  Books in languages ​​that are not in the language collection are ignored when bulk downloading  
  If the deduplication function is enabled, it will be filtered according to the priority of the language collection. For example, the language filter is `english, japanese`. If the page contains both the Japanese original and the localized version of the same book, the english version will be downloaded first. And if the order is reversed `japanese, english` download the original Japanese  
  Common language strings are english japanese english
- **Use proxy**  
  Support for using HTTP or SOCKS proxy, ie small aircraft can be used  
  The input format is `<protocol>://[username:password@]<IP>:<port>`, for example:
  - `http://user:passwd@127.0.0.1:1080`
  - `socks://127.0.0.1:1080` (If you use a small plane, fill in this directly, unless you change the local port)

## Use

### Download a single book

```bash
nhder https://nhentai.net/g/255771/
# or
nhder 255771
```

### Download a book in a category or search page

```bash
# Default will download the first page
nhder https://nhentai.net/tag/vanilla/
nhder https://nhentai.net/search/?q=c94+english

# If the link you copied is over the page, then the page you flipped to will be downloaded, which is determined by the page field.
# Note that if there is an ampersand in the link, there may be errors in some command lines. It is recommended to use English double quotes to wrap the link.
nhder https://nhentai.net/tag/vanilla/?page=2
nhder "https://nhentai.net/search/?q=vanilla+english&page=2"
```

Alternatively, you can use the `-p` parameter to specify all or part of the page for this pageable page, ignoring the page field in the link.

The format is `-p start:end`, which means that all the pages of the [start, end] page number range are downloaded; the start and end can be left blank, the default start is 1, and the end is the maximum number of pages; `-p` and ` -p :` is the same, but if you use `-p` directly, you must put it at the very end of the command.

```bash
#Pages 10:20
nhder https://nhentai.net/tag/vanilla/ -p 10:20

#Pages 1:15
nhder https://nhentai.net/tag/vanilla/ -p :15

# Download 30 pages ~ last
nhder https://nhentai.net/tag/vanilla/ -p 30:

# Download all
nhder https://nhentai.net/tag/vanilla/ -p
```

### Combination download

You can fill in multiple URLs or gids and the program will download them sequentially.

```bash
nhder 255771 256015 https://nhentai.net/tag/vanilla/ https://nhentai.net/search/?q=fgo
```

You can also use the `-p` parameter mentioned above and it will work for all categories or search page input.

## Other
This program supports multiple running instances.
Processing the same id accross two instances is unsupported.
