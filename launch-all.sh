#!/bin/bash

n_max=$((python n_max.py) 2>&1 >/dev/null)
n_low=1

while [ $n_max -gt $n_low ]; do
    ./launch-logic.sh ${n_max}
    imported=$(python n_import.py ${n_max} 2>&1 >/dev/null)
    echo "Imported" $imported
    n_max="$(($n_max - 1))"
done
echo "Kansei shita!"