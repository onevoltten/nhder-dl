require('colors');
const Config = require('./config');
const Fs = require('fs');
const Fse = require('fs-extra');
const Path = require('path');
const MultiThread = require('./multi-thread');

const RETRY_MAX = 10;

class NHDownloader {
	/**
	 * Creates an instance of NHDownloader.
	 * @param {Config} conf Configuration
	 * @param {*} [agent=null] Proxy
	 * @memberof NHDownloader
	 */
	constructor(conf, agent = null) {
		this.config = conf;
		if (agent) this.Axios = require('axios').create({
			httpsAgent: agent
		});
		else this.Axios = require('axios');
	}

	/**
	 * Axios download to file
	 *
	 * @param {string} dir Download directory
	 * @param {string} filename file name
	 * @param {string} url download link
	 * @param {*} [axiosOption={}] Axios option
	 * @returns Download to file Promise
	 * @memberof NHDownloader
	 */
	async axiosToFile(dir, filename, url, axiosOption = {}) {
		let response;
		axiosOption.responseType = 'stream';
		await this.Axios.get(url, axiosOption).then(res => response = res);
		return new Promise((reslove, reject) => {
			response.data.pipe(Fse.createWriteStream(Path.join(dir, filename)));
			response.data.on('end', () => {
				reslove(response);
			});
			response.data.on('error', e => {
				reject(e);
			});
		});
	}

	/**
	 * Download this book
	 *
	 * @param {*} book Book
	 * @param {number} [current=1] Current overall progress
	 * @param {number} [total=1] Total progress
	 * @returns Multi-threaded Promise.all
	 * @memberof NHDownloader
	 */
	download(book, current = 1, total = 1) {
		let config = this.config.getConfig();
		let tmpDirRoot = this.config.getTempDir();
		let {
			id,
			media_id,
			title,
			title_dir,
			num_pages,
			pages
		} = book;

		console.log('\n' + String(current).green + `/${total}\t` + `[${id}] `.gray + title + ` (${num_pages} pages)`.yellow);

		let tmpDir = Path.join(tmpDirRoot, `${id}`);
		let dlDir = Path.join(config.path, title_dir);
		let cpDir = Path.join(config.completed);

		Fse.ensureDirSync(tmpDir);
		Fse.ensureDirSync(dlDir);
		Fse.ensureDirSync(cpDir);
		
		//Save json book
		var jsonPath = book.id + '.json';
		try {
			if (! Fs.existsSync(dlDir + '/' + jsonPath)) {
				Fse.writeFileSync(dlDir + '/' + jsonPath, JSON.stringify(book, null, 1));
				console.log('Saved json: '.green + jsonPath)
			}else{
				console.log('Exists json: '.grey + jsonPath)
			}
		} catch (e) {
			console.log(e);
			process.exit()
		}
		//End

		let multiThread = new MultiThread(pages, config.thread);
		return multiThread.run((threadID, filename, index, total) => new Promise(async (resolve, reject) => {
			let url = `https://i.nhentai.net/galleries/${media_id}/${filename}`;
			let strTemplet = `\t${String(index).green}/${total}\t${url}`;

			console.log(`  [${threadID}]${strTemplet}`);

			let success = false;
			for (let retry = 0; retry < RETRY_MAX && !success; retry++) {
				await this.axiosToFile(tmpDir, filename, url, {
					timeout: 1000 * config.timeout
				}).then(async res => {
					//File integrity check
					let fileSize = res.headers['content-length'];
					let dlFile = Path.join(tmpDir, filename);
					let waitCnt = 0;
					do {
						waitCnt++;
						await sleep(500);
					} while (!Fse.existsSync(dlFile) && waitCnt <= 10); //Unknown bug handling
					let dlFileSize = Fse.statSync(dlFile).size;
					if (dlFileSize == fileSize) {
						Fse.moveSync(dlFile, Path.join(dlDir, filename));
						success = true;
					} else {
						Fse.unlinkSync(dlFile);
						throw new Error('Incomplete download.');
					}
				}).catch(e => {
					success = false;
					console.error(`${e}`.red);
					if (retry + 1 < RETRY_MAX)
						console.log('  ' + `[${threadID}]`.bgYellow + strTemplet);
					else
						console.log('  ' + `[${threadID}]`.bgRed + strTemplet);
				});
			}

			success ? resolve() : reject('Max retry.');
		}), 100);
	}
}

function sleep(ms) {
	return new Promise(resolve => {
		setTimeout(resolve, ms);
	});
}

module.exports = NHDownloader;
