const getLatestVersion = require('latest-version');
const compareVersions = require('compare-versions');
const Fse = require('fs-extra');
const Path = require('path');

const configFileDir = require('appdata-path').getAppDataPath('nhder');
const checkLogFile = Path.join(configFileDir, 'update.json');
const {
	name,
	version
} = require('../package.json');

/**
 * Initialize update check record
 *
 */
function init() {
	if (!Fse.existsSync(checkLogFile)) Fse.writeJSONSync(checkLogFile, {
		lastCheck: 0,
		latestVersion: '0'
	});
}

/**
 * Check for updates
 *
 * @returns The new version number, or null if there is no update
 */
async function check() {
	let {
		lastCheck,
		latestVersion
	} = Fse.readJSONSync(checkLogFile);
	const now = new Date().getTime();
	if (now > lastCheck + 3 * 24 * 60 * 60 * 1000) {
		latestVersion = await getLatestVersion(name);
		Fse.writeJSONSync(checkLogFile, {
			lastCheck: now,
			latestVersion
		});
	}
	if (compareVersions(latestVersion, version) > 0) return latestVersion;
	return null;
}

module.exports = {
	init,
	check
};
