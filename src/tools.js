const Fs = require('fs');
const Fse = require('fs-extra');
const Path = require('path');

const gReg = /^https?:\/\/nhentai\.net\/g\/([0-9]+)\/?$/;
const tagReg = /^https?:\/\/nhentai\.net\/[^\/?=]+\/([^\/?=]+)(\/(\?page=([0-9]+))?)?$/;
const searchReg = /^https?:\/\/nhentai\.net\/search\/\?q=([^&]+)()(&page=([0-9]+))?$/;


/**
 * Parse the URL entered by the user
 *
 * @param {string} url nhentai URL
 * @returns Analysis result
 */
function parseNHURL(url) {
	let type, value, page;
	let search = gReg.exec(url);
	if (search) {
		type = 'g';
		value = parseInt(search[1]);
	} else {
		search = tagReg.exec(url) || searchReg.exec(url);
		if (search) {
			type = 's';
			value = search[1];
			page = search[4];
		} else {
			return false;
		}
	}
	return {
		type,
		value,
		page: parseInt(page) || false
	};
}

/**
 * Filter the book
 *
 * @param {Array} bookList Subarray
 * @param {boolean} deduplication De-weighting
 * @param {Array} languages Language
 * @returns Filtered subarray
 */
function bookFilter(bookList, deduplication, languages) {

	let prettySet = [];
	let temp = [];
	let result = [];

	//Language filtering
	if (languages.length > 0) {
		for (let book of bookList) {
			if (languages.includes(book.language)) temp.push(book);
		}
	} else temp = [].concat(bookList);

	//De-weighting
	if (deduplication) {
		//Language priority
		if (languages.length > 1) {
			let langSet = [];
			for (let index in languages) {
				langSet[languages[index]] = index;
			}
			temp.sort((a, b) => {
				let compare = a.title_pretty.localeCompare(b.title_pretty);
				if (compare == 0)
					return langSet[a.language] - langSet[b.language];
				else return compare;
			});
		}
		//De-weighting
		for (let book of temp) {
			let pretty = book.title_pretty;
			if (prettySet[pretty]) continue;
			prettySet[pretty] = true;
			result.push(book);
		}
	} else result = temp;

	return result.sort((a, b) => parseInt(b.id) - parseInt(a.id));
}

/**
 * Filter downloaded books and pages
 *
 * @param {Array} bookList This sublist
 * @param {string} dlDirRoot Download directory
 * @returns Filtered subarray
 */
function downloadedFilter(bookList, dlDirRoot, dlDirCompleted) {
	let result = [];
	let fileList = Fse.readdirSync(dlDirRoot);
	for (let book of bookList) {
		let {
			title_dir,
			pages
		} = book;

		//Check folder
		// console.log(title_dir);
		// console.log(dlDirRoot);

		if (fileList.includes(title_dir)) {
			let pageList = Fse.readdirSync(Path.join(dlDirRoot, title_dir));
			let pageResult = [];

			//Check page
			for (let page of pages) {
				if (!pageList.includes(page)) pageResult.push(page);
			}
			if (pageResult.length == 0) continue; //Description of this book complete download
			//Use the undownloaded page list as this subpage list

			book.pages = pageResult;
		}

		result.push(book);
	}
	return result;
}

module.exports = {
	parseNHURL,
	bookFilter,
	downloadedFilter
};
