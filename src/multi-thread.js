function sleep(ms) {
	return new Promise(resolve => {
		setTimeout(resolve, ms);
	});
}

/**
 * Pseudo-multithreading
 *
 * @class MultiThread
 */
class MultiThread {
	/**
	 * Creates an instance of MultiThread.
	 * @param {Array} taskList Task parameter queue
	 * @param {number} threadNum Threads
	 * @memberof MultiThread
	 */
	constructor(taskList, threadNum) {
		this.tasks = taskList.slice();
		this.num = threadNum;
		this.hasRun = false;
	}

	/**
	 *	Execute multithreading
	 *
	 * @param {Function} promiseFunc Function to create a Promise instance
	 * @param {number} [interval=0] Thread establishment interval
	 * @returns Promise.all
	 * @memberof MultiThread
	 */
	async run(promiseFunc, interval = 0) {
		if (this.hasRun) return null;
		this.hasRun = true;
		let threads = [];
		let taskIndex = 0;
		//Create thread
		for (let threadID = 0; threadID < this.num; threadID++) {
			threads.push(new Promise(async resolve => {
				while (true) {
					let i = taskIndex++;
					if (i >= this.tasks.length) break;
					await promiseFunc(threadID, this.tasks[i], i + 1, this.tasks.length);
				}
				resolve();
			}));
			await sleep(interval);
		}
		return Promise.all(threads);
	}
}

module.exports = MultiThread;
