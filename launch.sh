#!/bin/bash
# 140,000-200,000 is approx 2tb
cd /home/${USER}/nhder/nhder/

g=284333
while [ $g -gt 1 ]; do
    ./launch-logic.sh ${g}
    imported=$(python n_import.py ${g} 2>&1 >/dev/null)
    echo "Imported" $imported
    ((g--))
done
echo "Kansei shita!"
