#!/bin/bash

# Converts png, static gif to jpg
# jpeg-recompress reduce filesize
# Move folder upper directory

function processM() {
    # Process PNG
    count=$(ls -1 ${D}/*.png 2>/dev/null | wc -l)
    if [ $count != 0 ]; then
        # Convert png to jpg
        echo "Convert PNG to JPG... [${D}]"
        ls -1 "${D}"*.png | parallel --will-cite -j 3 convert '{}' '{.}.jpg'
        # Delete png
        ls -1 "${D}"*.png | while read f; do rm "${f}"; done
    fi

    # Validate if gif is animated
    for gif in "${D}"*.gif; do
        if [ -f "${gif}" ]; then
            if [[ $(identify "${gif}" | wc -l) -gt 1 ]]; then
                # Animated gif
                echo "Valid GIF [${gif}]"
            else
                # Static gif
                echo "Convert GIF to JPG [${gif%.*}.jpg]"
                convert "${gif}" "${gif%.*}.jpg"
                # Delete gif
                rm "${gif}"
            fi
        fi
    done

    # Compress
    echo "Compress JPG... [${D}]"
    # ls -1 "${D}"*.jpg | while read f; do jpeg-recompress -n 50 -q medium -s -c -l 3 "${f}" "${f}"; done
    jpegoptim -s ${D}*.jpg

    echo "Completed! [${D}] [${D}../..]"
    # Move upper directory
    mv "${D}" "${D}../.."
}

Temp="/home/${USER}/nhder/nhder/download/temp/${1}"

if [ -n "$1" ]; then
    D="${Temp}"/
    processM "${1}"
else
    for D in "${Temp}"*/; do
        if [[ -d "${D}" ]]; then
            processM
        fi
    done
fi
